extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	Globals.ui.player_health.value = $Player.health
	GLAudioPlayer.play_music(load('res://assets/music/fight.wav'))
	add_child(load("res://ui/level_start_overlay.tscn").instantiate())
