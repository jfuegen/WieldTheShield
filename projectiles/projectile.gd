extends Node2D

class_name Projectile

var damage = 1
var speed = 5
var blockable = true
var homing = false :
	set(new_homing):
		var timer = Timer.new()
		add_child(timer)
		timer.start(1)
		timer.timeout.connect(_now_homing)
var exploding = false
var reflected = false

var mine = false : 
	set(exploding):
		mine = exploding
		self.exploding = exploding
		_set_and_scale_nodes()	
		var timer = Timer.new()
		add_child(timer)
		timer.start(4)
		timer.timeout.connect(_explode)

var target: Vector2 = Vector2(0,0)

func _ready():
	_set_and_scale_nodes()

func _process(_delta):
	rotation = get_angle_to(to_global(target.normalized() * 10)) + deg_to_rad(90)
	if !homing:
		if !mine:
			position += speed * target.normalized()
	elif homing:
		position += speed * to_local(Globals.player.position).normalized()

func _set_and_scale_nodes():
	if mine:
		$Sprite2D.texture = load("res://assets/projectiles/mine.svg")
	else:
		$Sprite2D.scale = Vector2(20,40) / $Sprite2D.texture.get_size()
	$Area2D/CollisionShape2D.shape.size = $Sprite2D.texture.get_size() * $Sprite2D.scale

func _on_area_2d_body_entered(body):
	if body.is_in_group('player') or (body.is_in_group('boss') and reflected):
		if !reflected:
			GLAudioPlayer.play_sound(load('res://assets/sounds/hit.wav'))
		body.take_damage(self)
		if exploding:
			_explode()
		queue_free()
	if body.is_in_group('shield') and blockable:
		target = target.normalized().bounce(position.normalized())
		homing = false
		reflected = true
		GLAudioPlayer.play_sound(load("res://assets/sounds/block.wav"))

func _now_homing():
	pass # TODO

func _explode():
	var t = load("res://projectiles/explosion.tscn").instantiate()
	t.position = self.position
	get_parent().add_child(t)
	queue_free()


func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()
