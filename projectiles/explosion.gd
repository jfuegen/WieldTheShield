extends Sprite2D

func _ready():
	GLAudioPlayer.play_sound(load("res://assets/sounds/explosion.wav"))
	modulate = Color.BEIGE
	modulate.a = 0.9
	_animate()
	
func _animate():
	var tween = create_tween()
	tween.tween_property(self,'modulate:a',1, 0.4)
	tween.tween_property(self,'modulate', Color.INDIAN_RED,1.6)
	tween.parallel().tween_property(self,'modulate:a',0, 1.6)

func _on_duration_timeout():
	queue_free()
