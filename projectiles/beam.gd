extends Node2D

class_name Beam

var damage:int = 5
var particles: Array
var on_cooldown = false
var can_damage = false

var target = Vector2(0,0) : 
	set(new_target):
		target = new_target.normalized() * Vector2(get_window().size)
		$RayCast2D.target_position = target
		$RayCast2D/Line2D.points = [position, $RayCast2D.target_position]
		_wind_up_animation()

func _process(_delta):
	var body = $RayCast2D.get_collider()
	if body and body.is_in_group("player") and !on_cooldown and can_damage:
		target = to_global(body.position)
		body.take_damage(self)
		on_cooldown = true

func _ready():
	particles = [$OriginParticles, $BeamParticles]
	GLAudioPlayer.play_sound(load("res://assets/sounds/laser.wav"))

# Animates the Beams wind-up animation
func _appear():
	$RayCast2D/Line2D.default_color = Color.ORANGE_RED
	$RayCast2D/Line2D.width = 20
	$BeamParticles.position = target / 2
	$BeamParticles.rotation = atan2(target.y - position.y, target.x - position.x)
	$BeamParticles.process_material.emission_box_extents.x = target.x / 2
	$BeamParticles.process_material.emission_box_extents.y = $RayCast2D/Line2D.width
	$BeamParticles.visible = true
	$OriginParticles.visible = true

func _wind_up_animation():
	var wind_up_time = 0.5
	var tween = create_tween()
	$WindUp.start(wind_up_time)
	$BeamParticles.hide()
	$OriginParticles.hide()
	tween.tween_property($RayCast2D/Line2D, 'width', 2, wind_up_time)
	tween.parallel().tween_property($RayCast2D/Line2D, 'default_color', Color.RED, wind_up_time)

func _on_duration_timeout():
	queue_free()

func _on_damage_cooldown_timeout():
	on_cooldown = false

func _on_wind_up_timeout():
	$Duration.start()
	can_damage = true
	_appear()
	
