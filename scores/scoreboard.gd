extends Node2D

@onready var ui_theme = preload("res://ui/ui.tres")

@onready var score_container = $ScrollContainer/MarginContainer/ScoreContainer
@onready var submit_button = $HBoxContainer/SubmitButton
@onready var contender_textedit = $HBoxContainer/ContenderTextEdit
@onready var result_label = $ResultLabel


@onready var submitted = false: 
	set(value):
		submitted = value
		submit_button.disabled = value
@onready var current_score = 0:
	set(value):
		current_score = value
		result_label.set_text("%d" % value)
		
@onready var scores : Dictionary = {}

func _ready():
	generate_scoreboard()

func set_score(contender, score):
	scores[contender] = score
	sort_dict(scores)
	
func sort_dict(dict: Dictionary) -> void:
	var pairs = dict.keys().map(func (key): return [key, dict[key]])
	pairs.sort_custom(func(a,b): return a[1] > b[1])
	dict.clear()
	for p in pairs:
		dict[p[0]] = p[1]

func clear_scoreboard():
	for n in score_container.get_children():
		score_container.remove_child(n)
		
func generate_scoreboard():
	var entries = []
	
	var contender_title = Label.new()
	var score_title = Label.new()
	
	contender_title.set_text("Contender")
	score_title.set_text("Score")
	
	for header in [contender_title, score_title]:
		header.add_theme_font_size_override("font_size", 32)
		header.add_theme_color_override("font_color", Color8(221, 32, 48, 255))
		
	entries += [contender_title, score_title]
		
	sort_dict(scores)
	for key in scores.keys():
		var contender_label = Label.new()
		var score_label = Label.new()
		
		score_label.set_text("%d" % scores[key])
		contender_label.set_text(key)
		
		entries += [contender_label, score_label]
	
	for i in entries.size():
		var entry : Label = entries[i]
		entry.theme = ui_theme
		score_container.add_child(entry)
		
		if i % 2 == 1:
			entry.set_horizontal_alignment(HORIZONTAL_ALIGNMENT_RIGHT)
		
func _on_submit_button_pressed():
	var contender = contender_textedit.text
	if contender != "":
		var updated_score = max(current_score, scores.get(contender, -1))
		set_score(contender, updated_score)
		submitted = true
		
		clear_scoreboard()
		generate_scoreboard()
