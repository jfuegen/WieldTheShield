extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().paused = true
	$ReadyTimer.start(2)
	$Ready.visible = true
	$Fight.hide()

func _on_ready_timer_timeout():
	$FightTimer.start(1)
	$Ready.hide()
	$Fight.visible = true

func _on_fight_timer_timeout():
	get_tree().paused = false
	queue_free()
