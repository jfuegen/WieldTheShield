extends Control

var goose_count = 0

enum MenuState {
	GAME,
	START,
	PAUSE,
	CREDITS,
	TUTORIAL,
	SETTINGS,
	END
}
signal menustate_changed(old_state, new_state)

var state = MenuState.START: 
	set(value):
		menustate_changed.emit(state, value)
		state = value

@onready var boss_health = $CanvasLayer/GameScreenBackground/BossHealth
@onready var player_health = $CanvasLayer/GameScreenBackground/PlayerHealth
@onready var score_label = $CanvasLayer/GameScreenBackground/ScoreLabel
@onready var scoreboard = $CanvasLayer/EndScreenBackground/Scoreboard

@onready var mapping = {
	MenuState.GAME : $CanvasLayer/GameScreenBackground,
	MenuState.START : $CanvasLayer/StartScreenBackground,
	MenuState.PAUSE : $CanvasLayer/PauseScreenBackground,
	MenuState.CREDITS : $CanvasLayer/CreditsBackground,
	MenuState.TUTORIAL : $CanvasLayer/TutorialBackground,
	MenuState.SETTINGS : $CanvasLayer/SettingsBackground,
	MenuState.END : $CanvasLayer/EndScreenBackground
}


var duck_sound = load("res://assets/sfx/duck.mp3")
var goose_scream = load("res://assets/sfx/goose_scream.mp3")
var goose_music = load("res://assets/music/Goose Anthem.mp3")

func _process(delta):
	get_viewport().gui_get_focus_owner()
	if goose_count == 3:
		goose_count = 0
		easter_egg()
	
func easter_egg():
	await get_tree().create_timer(1).timeout
	GLAudioPlayer.play_sound(goose_scream)
	await get_tree().create_timer(1).timeout
	$Background/CanvasLayer/StartParallax.hide()
	$CanvasLayer/StartScreenBackground/TitleLabel.text = "Wield the Goose"
	$CanvasLayer/StartScreenBackground/TitleLabel.add_theme_color_override("font_color",Color("#FF6600") )
	$Background/CanvasLayer/GooseParallax.show()
	$GoosePlayer.autoplay = true
	$GoosePlayer.play()
	
		
func _ready():
	pass

func transition_to(next_state:MenuState, try_hide=true):
	if try_hide:
		for background in mapping.values():
			background.visible = false
	mapping.get(next_state).visible = true
	state = next_state

func update_score(new_score):
	score_label.text = "Score: %d" % new_score
	
func update_player_health(new_hp):
	player_health.value = new_hp

func update_boss_health(max_hp,new_hp):
	var value = float(new_hp) / float(max_hp) * 100
	boss_health.value = value

func _on_start_button_pressed():
	$GoosePlayer.stop()
	Globals.main.start_game()
	transition_to(MenuState.GAME)

func _unhandled_input(event):
	var return_able_states = [MenuState.CREDITS,MenuState.TUTORIAL,MenuState.SETTINGS,MenuState.PAUSE]
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_ESCAPE:
			if state == MenuState.GAME:
				if get_tree().paused:
					get_tree().paused = false
					transition_to(MenuState.GAME)
				else:
					get_tree().paused = true
					transition_to(MenuState.PAUSE, false)
					$CanvasLayer/PauseScreenBackground/VBoxContainer/ContinueButton.grab_focus()
	if event is InputEventJoypadButton:
		if event.is_action_pressed("Pause"):
			if state == MenuState.GAME:
				if get_tree().paused:
					get_tree().paused = false
					transition_to(MenuState.GAME)
				else:
					get_tree().paused = true
					transition_to(MenuState.PAUSE, false)
					$CanvasLayer/PauseScreenBackground/VBoxContainer/ContinueButton.grab_focus()
		if return_able_states.has(state) and event.is_action_pressed("ui_cancel"):
			transition_to(MenuState.START)
			$CanvasLayer/StartScreenBackground/StartButton.grab_focus()
			

func _on_continue_button_pressed():
	get_tree().paused = false
	transition_to(MenuState.GAME)

func _on_quit_button_pressed():
	get_tree().paused = false
	Globals.main.stop_game()
	transition_to(MenuState.START)
	$CanvasLayer/StartScreenBackground/StartButton.grab_focus()

func _on_restart_button_pressed():
	Globals.main.start_game()
	transition_to(MenuState.GAME)
	

func _on_back_to_menu_button_pressed():
	transition_to(MenuState.START)
	$CanvasLayer/StartScreenBackground/StartButton.grab_focus()

func _on_credit_button_pressed():
	transition_to(MenuState.CREDITS)
	$CanvasLayer/CreditsBackground/Backbutton.grab_focus()

func _on_backbutton_pressed():
	transition_to(MenuState.START)
	$CanvasLayer/StartScreenBackground/StartButton.grab_focus()

func _on_tutorial_button_pressed():
	transition_to(MenuState.TUTORIAL)
	$CanvasLayer/TutorialBackground/Backbutton.grab_focus()
func _on_settings_button_pressed():
	transition_to(MenuState.SETTINGS)#
	$CanvasLayer/SettingsBackground/Backbutton.grab_focus()


func _on_creator_label_pressed():
	GLAudioPlayer.play_sound(load("res://assets/sfx/duck.mp3"))
	goose_count+=1
	$goose_quack.start(2)


func _on_level_input_spin_box_value_changed(value):
	Globals.main.level = value


func _on_sound_volume_slider_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sound"),value)


func _on_music_volume_slider_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"),value)


func _on_goose_quack_timeout():
	goose_count = 0



func _on_tutorial_button_focus_entered():
	$CanvasLayer/StartScreenBackground/EtcContainer/TutorialButton.modulate = Color("#2596be")
	

func _on_tutorial_button_focus_exited():
	$CanvasLayer/StartScreenBackground/EtcContainer/TutorialButton.modulate = Color(0.9,0.9,0.9)
	



func _on_credits_button_focus_entered():
	$CanvasLayer/StartScreenBackground/EtcContainer/CreditsButton.modulate = Color("#2596be")


func _on_credits_button_focus_exited():
	$CanvasLayer/StartScreenBackground/EtcContainer/CreditsButton.modulate = Color(0.9,0.9,0.9)


func _on_settings_button_focus_entered():
	$CanvasLayer/StartScreenBackground/EtcContainer/SettingsButton.modulate = Color("#2596be")


func _on_settings_button_focus_exited():
	$CanvasLayer/StartScreenBackground/EtcContainer/SettingsButton.modulate = Color(0.9,0.9,0.9)


