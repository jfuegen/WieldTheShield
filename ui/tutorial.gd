extends TextureRect

@onready var tutorial_bullet = $BulletDescription/TutorialBullet
@onready var tutorial_player = $BulletDescription/TutorialPlayer
@onready var tutorial_boss = $BossDescription/TutorialBoss
@onready var tutorial_shield = $ReflectDescription/TutorialShield

func _ready():
	animate_bullet()
	animate_player()
	animate_shield()
	animate_boss()

func animate_bullet():
	var origin = tutorial_bullet.position
	var tween = create_tween().set_parallel(false)
	tween.tween_property(tutorial_bullet, "position", origin + Vector2(100, 0), 2)
	tween.tween_property(tutorial_bullet, "position", origin, 0)
	tween.set_loops()
	
func animate_player():
	var origin = tutorial_player.position
	var tween = create_tween().set_parallel(false)
	var change =  Vector2(0, 20)
	tween.tween_property(tutorial_player, "position", origin + change, 0.5)
	tween.tween_property(tutorial_player, "position", origin - change, 0.5)
	tween.set_loops()

func animate_shield():
	var origin = tutorial_shield.rotation
	var tween = create_tween().set_parallel(false)
	var change =  PI / 5
	tween.tween_property(tutorial_shield, "rotation", origin + change, 1)
	tween.tween_property(tutorial_shield, "rotation", origin - change, 1)
	tween.set_loops()
	
func animate_boss():
	var origin = tutorial_boss.rotation
	var tween = create_tween().set_parallel(false)
	var change =  PI
	tween.tween_property(tutorial_boss, "rotation", origin + change, 1)
	tween.tween_property(tutorial_boss, "rotation", origin, 0)
	tween.set_loops()
