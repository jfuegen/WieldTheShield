extends Node2D

@onready var start_parallax = $CanvasLayer/StartParallax
@onready var game_parallax = $CanvasLayer/GameParallax

@onready var camera = $CanvasLayer/Camera2D
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	camera.position.x += 60 * delta


func _on_ui_menustate_changed(old_state, new_state):
	if new_state == Globals.ui.MenuState.START:
		start_parallax.visible = true
		game_parallax.visible = false
	if new_state == Globals.ui.MenuState.GAME:
		start_parallax.visible = false
		game_parallax.visible = true
