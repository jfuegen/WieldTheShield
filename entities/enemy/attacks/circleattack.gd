extends Attack

class_name CircleAttack

@export var radius : float = 10
@export var amount : int = 10
@export var offset : float = 0.1
	
func execute():
	for i in range(amount):
		var angle_i = i * (2 * PI) / amount + offset
		var cartesian =  Vector2(cos(angle_i), sin(angle_i))
		
		var b = projectile.instantiate()
		hook.add_child(b)
		b.target = cartesian
		b.homing = true
