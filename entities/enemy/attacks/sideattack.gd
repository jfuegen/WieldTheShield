extends Attack

class_name SideAttack

@export var amount = 2
@export var radius = 40
@export var offset = 0
@export var angle = PI/4

func execute():
	var m = Transform2D().rotated(angle)
	
	for j in range(2):
		for i in range(amount):
			var sig = (-1) ** (j % 2)
			if is_instance_valid(hook):
				var b = projectile.instantiate()
				hook.get_parent().add_child(b)
				b.target = m.basis_xform(Vector2(sig,0)) * 200
				b.position = hook.global_position
				b.mine = true
				
				
