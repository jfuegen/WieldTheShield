extends Attack

class_name SingleAttack

@export var segments = 1.9

var current_angle = 0.0

func execute():
	
	if is_instance_valid(hook):
		var b = load('res://projectiles/beam.tscn').instantiate()
		hook.add_child(b)
		var cartesian =  Vector2(cos(current_angle), sin(current_angle))
		b.target = cartesian
		
		current_angle = fmod(current_angle + segments, 2 * PI)
				
