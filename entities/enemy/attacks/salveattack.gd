extends Attack

class_name SalveAttack
@onready var attack = $attack
@onready var boss = preload("res://entities/enemy/boss.gd")
var start_angle = 0



var bullet_position: float = 0.0
@export_category("Main Category")
@export var counter_clockwise :bool = false
@export_range(0,360)var angle_between_bullets: float = 180
@export_range(1,100) var amount_projectile :int = 1
@export_range(0,2) var shoot_in_line_delay :float = 1
@export_range(0,1) var change_direction_delay :float = 1







func _get_vector(angle):

	bullet_position = angle + deg_to_rad(angle_between_bullets)
	#print(theta)
	return Vector2(cos(bullet_position),sin(bullet_position))

func execute():
		var direction = _get_vector(bullet_position)	
		for x in amount_projectile:	
			if is_instance_valid(hook):
				var projectile = projectile.instantiate()
				hook.add_child(projectile)
				projectile.target = direction
				
				await get_tree().create_timer(200).timeout
				if counter_clockwise:
					direction = _get_vector(bullet_position) * -1
					
		if is_instance_valid(hook):
						await hook.get_tree().create_timer(change_direction_delay).timeout
		
		




