extends CharacterBody2D

@onready var boss_sprite = $AnimatedSprite

var circleattack : Attack = CircleAttack.new(self)
var sideattack : Attack = SideAttack.new(self)
var singleattack : Attack = SingleAttack.new(self)

enum states {NORMAL,HARD,ENRAGED}
enum move_states {IDLE,FOLLOW,TELEPORT}

var state_patterns = {
	states.NORMAL: {
		"next_stage_amount": 30,
		"pattern": [circleattack,singleattack,circleattack,singleattack,circleattack],
		"timers": [0.2,0.2,0.2,0.2,0.2]
	},
	states.HARD: {
		"next_stage_amount": 20,
		"pattern": [circleattack,circleattack,sideattack,sideattack,sideattack,circleattack,circleattack,circleattack],
		"timers": [0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1]
	},
	states.ENRAGED: {
		"next_stage_amount": 50,
		"pattern": [circleattack,circleattack,circleattack,circleattack,sideattack,
		sideattack],
		"timers": [0.2,0.2,0.1,0.1,0.1,0.1]
	}
}

var current_state = states.NORMAL
var current_movement = move_states.IDLE
var current_pattern_index = 0
var attack_counter = 0

var max_health = 100
var health = 100
var speed = 0.4

func _ready():
	attack_pattern()
	$MovementStateTimer.start()
	boss_sprite.play("default")
	
func _process(delta):
	if current_movement == move_states.FOLLOW:
		var direction = Globals.player.position - position
		position += direction * delta * speed
	if current_movement == move_states.TELEPORT:
		current_movement = get_next_move_state()	
		do_teleport()
	if health <= 0:
		Globals.main.boss_defeated()
		
	for child in get_children():
		if child.name.contains("Sprite2D"):
			child.look_at(Globals.player.position)

func attack_pattern():
	var current_stage_info = state_patterns.get(current_state)
	# Check and do state change if can
	if attack_counter >= current_stage_info.get("next_stage_amount"):
		current_state = get_next_state()
		attack_counter = 0
		current_pattern_index = 0
		if current_state == states.ENRAGED:
			animate_rage(5, 2)
			
		
	# Get the next attack and timer		
	var attack = current_stage_info.get("pattern")[current_pattern_index]
	var timer = current_stage_info.get("timers")[current_pattern_index]
	attack.execute()
	attack_counter += 1
	var timer_instance = $WaveShotTimer
	timer_instance.wait_time = timer
	timer_instance.start()

func take_damage(projectile):
	health = health - projectile.damage
	Globals.ui.update_boss_health(max_health,health)
	Globals.main.score += 1
	Globals.ui.update_score(Globals.main.score)


func _on_wave_shot_timer_timeout():
	$WaveShotTimer.stop()
	current_pattern_index = (current_pattern_index + 1) % state_patterns.get(current_state).get("pattern").size()
	if health > 0:
		attack_pattern()
	
func get_next_state():
	return states.values()[(states.values().find(current_state) + 1) % states.size()]

func get_next_move_state():
	return move_states.values()[(move_states.values().find(current_movement) + 1) % move_states.size()]
	
func _on_movement_state_timer_timeout():
	if health <= 0:
		$MovementStateTimer.stop()
		return
	current_movement = get_next_move_state()	

func animate_rage(times=1, interval=1):
	var tween = get_tree().create_tween().set_parallel(false)
	for i in range(times):
		tween.tween_property(boss_sprite, "modulate", Color.RED, interval)
		tween.tween_property(boss_sprite, "modulate", Color.WHITE, interval)	
	tween.play()

func do_teleport():
	var origin_scale = boss_sprite.scale
	var teleport_position = Vector2(Globals.player.global_position)
	
	var tween = get_tree().create_tween().set_parallel(false)
	tween.tween_property(boss_sprite, "scale", Vector2(0,0), 1)
	tween.tween_callback(update_position.bind(teleport_position))
	tween.tween_property(boss_sprite, "scale", origin_scale, 1)
	tween.play()

func update_position(new_pos):
	global_position = new_pos

