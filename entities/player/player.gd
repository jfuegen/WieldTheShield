extends CharacterBody2D

@export var speed = 300.0
var health = 40
var controller_plugged_in =true 

func _ready():
	Globals.player = self
	if Input.get_connected_joypads().is_empty():
		controller_plugged_in =false


func _process(_delta):
	if controller_plugged_in:
		var lookdir = Input.get_vector("shield_move_left","shield_move_right","shield_move_up","shield_move_down")
		if lookdir != Vector2.ZERO:
			global_rotation = lerp_angle(global_rotation,lookdir.angle(),0.09)
		
	else:
		look_at(get_global_mouse_position())
	
	
func _physics_process(delta):
	var input = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	velocity = input * speed * delta * 60
	
	move_and_slide()
	
	# Clamp the position within the viewport bounds
	#position.x = clamp(position.x, 0 + get_tree().root.content_scale_size.x * 0.08, get_tree().root.content_scale_size.x * 0.92)
	#position.y = clamp(position.y, 0 + get_tree().root.content_scale_size.y * 0.08, get_tree().root.content_scale_size.y * 0.92)
	
	

func take_damage(projectile):
	health -= projectile.damage
	Globals.ui.update_player_health(health)
	if health <= 0:
		Globals.ui.scoreboard.current_score = Globals.main.score
		Globals.ui.scoreboard.submitted = false
		Globals.ui.transition_to(Globals.ui.MenuState.END, false)
		Globals.main.stop_game()		
