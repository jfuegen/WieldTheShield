extends Node

# Short for GooseLabsAudioPlayer
class_name GLAudioPlayer

# Creates a new AudioPlayer and automatically adds / removes it from the scene tree
static func play_sound(sound: Resource):
	var audio_stream = AudioStreamPlayer.new()
	Globals.main.add_child(audio_stream)
	audio_stream.stream = sound
	audio_stream.bus = "Sound"
	audio_stream.volume_db = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Sound"))
	audio_stream.play()
	audio_stream.finished.connect(GLAudioPlayer._destroy.bind(audio_stream))

# Creates a new AudioPlayer which automatically plays the provided Resource
# Does not delete itself and plays continuesly, the provided Resource must be set to looping
static func play_music(music: Resource):
	var audio_stream = AudioStreamPlayer.new()
	audio_stream.autoplay = true
	audio_stream.bus = "Music"
	audio_stream.stream = music
	audio_stream.volume_db = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Music"))
	Globals.main.add_child(audio_stream)
	
static func _destroy(stream):
	Globals.main.remove_child(stream)

