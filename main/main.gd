extends Node

var player_scene = preload("res://entities/player/player.tscn")
var boss_scene = preload("res://entities/enemy/boss.tscn")
@onready var playground = $playground

var player = null
var boss = null
var score = 0
var level = 1:
	set(value):
		level = int(value)

func _ready():
	Globals.ui = $UILayer/Ui
	Globals.main = self
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sound"),-15)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"),-15)

func stop_game():
	for child in playground.get_children():
		playground.remove_child(child)
	
func start_game():
	score = 0
	Globals.ui.update_score(0)
	_next_level()
	
func boss_defeated():
	for child in playground.get_children():
		playground.remove_child(child)
	
	if level < 3:
		level += 1
	else:
		level = 1
	_next_level()

func _next_level():
	var level_path = "res://levels/level_" + str(level) + ".tscn"
	var level_scene = load(level_path).instantiate()
	playground.add_child(level_scene)
